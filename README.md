# golang-project-template

## Gitpod

This project is a [Gitpod](https://gitpod.io) project: if you open this project with GitPod, you'll get an entire GoLang & TinyGo toolchain.

## Tools

If you want to generate a **Go WASM** project for the **browser**, run the VSCode Task: `Tasks: Run Task`, choose `generate go web project` and give a name to the project (it will generate a new directory).

If you want to generate a **TinyGo WASM** project for the **browser**, run the VSCode Task: `Tasks: Run Task`, choose `generate go web project` and give a name to the project (it will generate a new directory).

## OpenVSCode

If you prefer working locally, you can use the [OpenVSCode project](https://www.gitpod.io/blog/openvscode-server-launch)

Run the below command:
```bash
docker run -it --init -p 3000:3000 -p 8080:8080 k33g/openvscode-go:0.0.4
``` 

Then clone this project and install the Golang VSCode extensions.
> https://gitlab.com/wasmcooking/golang-project-template.git

Once the project cloned:
- Run the VSCode Task: `Tasks: Run Task`, choose `openvscode init go env`
- Install the recommended VSCode extensions
- Run `npm install`






