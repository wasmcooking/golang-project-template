package main

import (
	"fmt"
	"os"
)

func main() {
	argsWithoutProg := os.Args[1:]
	argsWithProg := os.Args

	fmt.Println("👋 Hello World 🌍")

	fmt.Println(argsWithProg)
	fmt.Println(argsWithoutProg)

	<-make(chan bool)
}
