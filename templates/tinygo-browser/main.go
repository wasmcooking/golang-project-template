package main

import (
	"fmt"
  "os"
)

func main() {
	fmt.Println("👋 Hello World 🌍")
  fmt.Println("MESSAGE:", os.Getenv("MESSAGE"))

	<-make(chan bool)
}
